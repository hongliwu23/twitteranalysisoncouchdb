from flask import Flask, render_template, request, jsonify, url_for
from flask_googlemaps import GoogleMaps
from flask_googlemaps import Map, icons
import couchdb

app = Flask(__name__, static_folder = "static", template_folder="templates")

# you can set key as config
app.config['GOOGLEMAPS_KEY'] = "AIzaSyAwpmwV9Ywax3DCBBJO_f82T-le0wF6vt0"

# you can also pass key here
GoogleMaps(app, key="AIzaSyAwpmwV9Ywax3DCBBJO_f82T-le0wF6vt0")


@app.route('/index')
def index():
    # senario=readSenario()
    return render_template('index.html'
        # ,senario=senario
        )

# create a map with given map name, city coordinates and markers
def Createmap(city,location,markers):
    onemap = Map(
            identifier=city,
            varname=city,
            style=(
                "height:300px;"
                "width:500px;"
                # "top:0;"
                # "left:0;"
                # "position:absolute;"
                "z-index:200;"
            ),
            markers=markers,
            lat=location[0],
            lng=location[1],
            zoom=10,
            zoom_control=False,
            maptype_control=False,
            scale_control=False,
            streetview_control=False,
            rotate_control=False,
            fullscreen_control=False
        )
    return onemap

# get senario names from a txt document
def readSenario():
    with open("senario.txt", 'r') as data:
        senario = []
        for line,info in enumerate(data):
                if(info[-1:]=='\n'):
                    info = info[:-1]
                info = info.split('@')
                if(len(info)==2):
                    theme = info[0]
                    senario.append(str(theme))
    return senario

# connect to the database
def connect():
    admin = 'team22'
    pswd = 'group22'
    serverAddress = "http://" + admin + ":" + pswd + "@115.146.90.181:5984"
    couch = couchdb.Server(serverAddress)
    # couch = couchdb.Server("http://team22:group22@localhost:5984")
    db = couch['twitter']
    return db,couch
    
def getAurinDB(couch,name):
    db = couch['aurin_data_'+name+"_victoria"]
    return db

def getTweetDB(coucn):
    db = couch['twitter']
    return db,couch

#  a map shows the top 5 topic in 4 cities
@app.route('/')
@app.route('/topic')
def topic():
    # connect to database
    db,couch = connect()
    # get senarios for searching in another page
    # senario = readSenario()
    # get top topics in different city
    melb,melbcount = getTopTopic(db,'Melbourne')
    sydn,sydncount = getTopTopic(db,'Sydney')
    pert,pertcount = getTopTopic(db,'Perth')
    adel,adelcount = getTopTopic(db,'Adelaide')
    # set markers
    markers = []
    markers = addTopicMarker(markers,'Melbourne',melb,melbcount,[-37.8136,144.9630])
    markers = addTopicMarker(markers,'Sydney',sydn,sydncount,[-33.8599,151.2111])
    markers = addTopicMarker(markers,'Perth',pert,pertcount,[-31.9522,115.8589])
    markers = addTopicMarker(markers,'Adelaide',adel,adelcount,[-34.9290,138.6010])
    # draw a map
    topicmap = setTopicMap(markers)
    # print(melb)
    return render_template('topic.html',
        topicmap = topicmap,
        melbchartname = melb, melbchartvalue = melbcount, sydnchartname=sydn,sydnchartvalue=sydncount)
        # senario=senario)

# set markers for the map
def addTopicMarker(markers,name,city,count,location):
    markers.extend([{
            'lat':location[0],
            'lng':location[1],
            'infobox':(
                "<h4>"+name+" Topic:</h4>"
                "<p>1."+city[0]+":"+str(count[0])+"</p>"
                "<p>2."+city[1]+":"+str(count[1])+"</p>"
                "<p>3."+city[2]+":"+str(count[2])+"</p>"
                "<p>4."+city[3]+":"+str(count[3])+"</p>"
                "<p>5."+city[4]+":"+str(count[4])+"</p>"
            )
    }])
    return markers

# create topic map with markers
def setTopicMap(markers):
    topicmap = Map(
            identifier="topicmap",
            varname="topicmap",
            style=(
            "height:500px;"
            "width:700px;"
            "z-index:200;"
            ),
            lat=-30.0000,
            lng=137.6010,
            zoom=4,
            markers = markers,
            zoom_control=False,
            maptype_control=False,
            scale_control=False,
            streetview_control=False,
            rotate_control=False,
            fullscreen_control=False
        )
    return topicmap

# get top 5 topic in one city
def getTopTopic(db,city):
    # read part of the view from database, only focus on one city
    view = db.view('_design/Tweet/_view/topics',group=True,startkey=[city],endkey=[city,{}])
    # the results
    topics=['','','','','']
    count = [0,0,0,0,0]
    # find through the view
    for row in view:
        # if some other city is included, data will not be handled
        if(row.key[0]==city):
            topic = row.key[1]
            num = row.value
            # put in the first value
            if(topics[0]==''):
                topics[0]=topic
                count[0]=num
            else:
                # compare with the values in the result 
                # to see whether this topic has greater value
                if(num>count[0]):
                    count[4]=count[3]
                    count[3]=count[2]
                    count[2]=count[1]
                    count[1]=count[0]
                    count[0]=num
                    topics[4]=topics[3]
                    topics[3]=topics[2]
                    topics[2]=topics[1]
                    topics[1]=topics[0]
                    topics[0]=topic
                else:
                    if(num>count[1]):
                        count[4]=count[3]
                        count[3]=count[2]
                        count[2]=count[1]
                        count[1]=num
                        topics[4]=topics[3]
                        topics[3]=topics[2]
                        topics[2]=topics[1]
                        topics[1]=topic
                    else:
                        if(num>count[2]):
                            count[4]=count[3]
                            count[3]=count[2]
                            count[2]=num
                            topics[4]=topics[3]
                            topics[3]=topics[2]
                            topics[2]=topic
                        else:
                            if(num>count[3]):
                                count[4]=count[3]
                                count[3]=num
                                topics[4]=topics[3]
                                topics[3]=topic
                            else:
                                if(num>count[4]):
                                    count[4]=num
                                    topics[4]=topic

    return topics,count

@app.route('/sentiment')
def sentiment():
    melbmap,sydnmap,pertmap,adelmap,theme,reply,view,melbchart,sydnchart,pertchart,adelchart = map('sentiment_analysis')
    return render_template('map.html',
        melbmap=melbmap,sydnmap=sydnmap,pertmap=pertmap,adelmap=adelmap,
        theme='Overall Sentiment Comparison',reply=reply,
        view=view,
        melbchart=melbchart,sydnchart=sydnchart,pertchart=pertchart,adelchart=adelchart
        # lable =lable,chart_value=chart_value,bgcolor=bgcolor,borcolor=borcolor
        )

@app.route('/senario1')
def senario1():
    melbmap,sydnmap,pertmap,adelmap,theme,reply,view,melbchart,sydnchart,pertchart,adelchart = map('job_hiring')
    return render_template('map.html',
        melbmap=melbmap,sydnmap=sydnmap,pertmap=pertmap,adelmap=adelmap,
        theme='Attitude Toward Job Hiring',reply=reply,
        view=view,
        melbchart=melbchart,sydnchart=sydnchart,pertchart=pertchart,adelchart=adelchart)

# show the senarios in some maps
# @app.route('/map/', methods=['POST','GET'])
def map(theme):
    # connect to database
    db,couch = connect()
    # get which senario to show
    # theme=request.form['theme']
    reply=''
    view =''
    # get senarios for searching in another page
    # senario = readSenario()
    # initialize the default result to show in the chart
    melbchart = ['Melbourne',0,0,0]
    sydnchart = ['Sydney',0,0,0]
    pertchart = ['Perth',0,0,0]
    adelchart = ['Adelaide',0,0,0]

    if(theme!=''): 
        # get the view depends on the senario
        view = db.view('_design/Tweet/_view/'+theme,group=True)
        # add markers for each city based on the view
        melbmarkers,sydnmarkers,pertmarkers,adelmarkers = ExtendMarker(view)
        # set value for charts
        melbchart = getCityCharts(melbmarkers,melbchart)
        sydnchart = getCityCharts(sydnmarkers,sydnchart)
        pertchart = getCityCharts(pertmarkers,pertchart)
        adelchart = getCityCharts(adelmarkers,adelchart)
        # draw the map with markers
        melbmap = Createmap('melbmap',[-37.8136,144.9630],melbmarkers)
        sydnmap = Createmap('sydnmap',[-33.8599,151.2111],sydnmarkers)
        pertmap = Createmap('pertmap',[-31.9522,115.8589],pertmarkers)
        adelmap = Createmap('adelmap',[-34.9290,138.6010],adelmarkers)
       
    else:
        # if the senario is not exist, show the default map
        melbmap = Createmap('melbmap',[-37.8136,144.9630],[])
        sydnmap = Createmap('sydnmap',[-33.8599,151.2111],[])
        pertmap = Createmap('pertmap',[-31.9522,115.8589],[])
        adelmap = Createmap('adelmap',[-34.9290,138.6010],[])

    return melbmap,sydnmap,pertmap,adelmap,theme,reply,view,melbchart,sydnchart,pertchart,adelchart
    # return render_template('map.html',
    #     melbmap=melbmap,sydnmap=sydnmap,pertmap=pertmap,adelmap=adelmap,
    #     theme=theme,reply=reply,
    #     view=view,senario=senario,
    #     melbchart=melbchart,sydnchart=sydnchart,pertchart=pertchart,adelchart=adelchart
    #     # lable =lable,chart_value=chart_value,bgcolor=bgcolor,borcolor=borcolor
    #     )

# set the value from markers to chart
def getCityCharts(markers,chart):
    for mark in markers:
        # print(chart[1])
        if(mark['lable'].split(' ')[0]=='positive'):
            chart[1]=int(mark['lable'].split(' ')[1])
        if(mark['lable'].split(' ')[0]=='negative'):
            chart[2]=int(mark['lable'].split(' ')[1])
        if(mark['lable'].split(' ')[0]=='neutral'):
            chart[3]=int(mark['lable'].split(' ')[1])
    return chart

# add markers to a map from data of view
def ExtendMarker(view):
    # initialze markers for cities
    melbmarkers = []
    sydnmarkers = []
    pertmarkers = []
    adelmarkers = []

    for row in view:
        # add a marker depend on the city
        if(row.key[0]=='Melbourne'):
            location = [-37.8136,144.9630]
            melbmarkers = newMarker(melbmarkers,location,row.key[1],row.value)
        if(row.key[0]=='Sydney'):
            location = [-33.8599,151.2111]
            sydnmarkers = newMarker(sydnmarkers,location,row.key[1],row.value)
        if(row.key[0]=='Perth'):
            location = [-31.9522,115.8589]
            pertmarkers = newMarker(pertmarkers,location,row.key[1],row.value)
        if(row.key[0]=='Adelaide'):
            location = [-34.9290,138.6010]
            adelmarkers = newMarker(adelmarkers,location,row.key[1],row.value)
    return melbmarkers,sydnmarkers,pertmarkers,adelmarkers

# add one marker for one city
def newMarker(markers,location,sentiment,value):
    # different sentiment havs different marker
    if(sentiment=="positive"):
        icon_str = 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=flag|bbT|Positive:'+str(value)+'|FFFFFF|008000'
        infobox_str = "<b style='color:green;'>Positive:"+str(value)+"</b>"
        markers.extend([{
                'icon':icon_str,
                'lat': location[0]+0.05,
                'lng': location[1]+0.1,
                'infobox':infobox_str,
                'lable':'positive '+str(value)
                }])
    else:
        if(sentiment=="negative"):
            icon_str = 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=flag|bbT|Negative:'+str(value)+'|FFFFFF|FF0000'
            infobox_str = "<b style='color:red;'>Negative:"+str(value)+"</b>"
            markers.extend([{
                    'icon':icon_str,
                    'lat': location[0]-0.05,
                    'lng': location[1]+0.1,
                    'infobox':infobox_str,
                    'lable':'negative '+str(value)
                    }])
        else:
            # icon_str = '../static/Neutral-48.png'
            icon_str = 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=flag|bbT|Neutral:'+str(value)+'|FFFFFF|F1C40F'
            infobox_str = "<b style='color:#ffcc00;'>Neutral:"+str(value)+"</b>"
            markers.extend([{
                    'icon':icon_str,
                    'lat': location[0],
                    'lng': location[1]-0.1,
                    'infobox':infobox_str,
                    'lable':'neutral '+str(value)
                    }])
    return markers

@app.route('/aurinEdu')
def aurinEdu():
    table,theme,reply,xlist = aurin('education')
    return render_template('aurin.html',
        # aurinmap=aurinmap,
        table=table,
        theme='Education',reply=reply,
        xlist = xlist)

@app.route('/aurinLang')
def aurinLang():
    table,theme,reply,xlist = aurin('language')
    return render_template('aurin.html',
        # aurinmap=aurinmap,
        table=table,
        theme='Language',reply=reply,
        xlist = xlist)

@app.route('/aurinAge')
def aurinAge():
    table,theme,reply,xlist = aurin('age_distribution')
    return render_template('aurin.html',
        # aurinmap=aurinmap,
        table=table,
        theme='Age Distribution',reply=reply,
        xlist = xlist)
    
# @app.route('/aurin/', methods=['POST','GET'])
def aurin(theme):
    # get senarios for searching in another page
    # theme=request.form['aurintheme']
    reply=''

    # connect to tweet database
    db,couch = connect()

    table = {}
    # get the view depends on the senario
    if(theme=='education' or theme=='language'):
        table,suburb_names = getAurinData(couch,theme)
        table = calulatePercentage(couch,theme,table,suburb_names)
        if(theme=='education'):
            xlist = getXY(table,'percentage','calculation')
        else:
            xlist= getXY(table,'calculation','percentage')
    else:
        table, suburb_names = getAurinDataAge(couch,theme)
        table = gatherValue(couch,theme,table,suburb_names)
        xlist = getXY(table,'A15_39','tweet_num')
    # senario = readSenario()
    # aurinmap = aurinMapMarkers([])
    return table,theme,reply,xlist
    # return render_template('aurin.html',
    #     # aurinmap=aurinmap,
    #     table=table,
    #     senario=senario,theme=theme,reply=reply,)

def getXY(table,xname,yname):
    #xlist = [ {'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0},{'x':0,'y':0}]
    xlist = []
    count = 0
    for item in table:
        d = {'x': table[item][xname], 'y': table[item][yname], 'SA2': item}
        xlist.append(d)
        #xlist[count]['x'] = table[item][xname]
        #xlist[count]['y'] = table[item][yname]
        count = count + 1
    xlist2 = sorted(xlist, key = lambda x:x['x'])
    return xlist2
    

# aurin age senario
def getAurinDataAge(couch,name):
    db = couch['aurin_data_'+name+"_victoria"]
    suburbs = [206041122,206041117,206071142,206051134,206061137,206061136,206051133,206051129,206031115,206051130]
    suburb_names = ['','','','','','','','','','']
    count = 0
    table={}
    for id in db:
        doc = db[id]
        if(doc['properties']['area_code'] in suburbs):
            area_name=doc['properties']['area_name']
            suburb_names[count] = area_name
            count = count+1
            temp = {
                'A0':round(float(doc['properties']['x0_4_p_3_percent_6_13_6_13']),2),
                'A5':round(float(doc['properties']['x5_9_p_3_percent_6_13_6_13']),2),
                'A10':round(float(doc['properties']['x10_14_p_3_percent_6_13_6_13']),2),
                'A15':round(float(doc['properties']['x15_19_p_3_percent_6_13_6_13']),2),
                'A20':round(float(doc['properties']['x20_24_p_3_percent_6_13_6_13']),2),
                'A25':round(float(doc['properties']['x25_29_p_3_percent_6_13_6_13']),2),
                'A30':round(float(doc['properties']['x30_34_p_3_percent_6_13_6_13']),2),
                'A35':round(float(doc['properties']['x35_39_p_3_percent_6_13_6_13']),2),
                'A40':round(float(doc['properties']['x40_44_p_3_percent_6_13_6_13']),2),
                'A45':round(float(doc['properties']['x45_49_p_3_percent_6_13_6_13']),2),
                'A50':round(float(doc['properties']['x50_54_p_3_percent_6_13_6_13']),2),
                'A55':round(float(doc['properties']['x55_59_p_3_percent_6_13_6_13']),2),
                'A60':round(float(doc['properties']['x60_64_p_3_percent_6_13_6_13']),2),
                'A65':round(float(doc['properties']['x65pl_p_3_percent_6_13_6_13']),2)
            }
            temp['A0_14'] = round(temp['A0'] + temp['A5'] + temp['A10'],2)
            temp['A15_39'] = round(temp['A15'] + temp['A20'] + temp['A25'] + temp['A30'] + temp['A35'],2)
            temp['A40_64'] = round(temp['A40'] + temp['A45'] + temp['A50'] + temp['A55'] + temp['A60'],2)
            table[str(area_name)]=temp
        # print(area_name)
    return table,suburb_names

#get tweets send in each sub
def gatherValue(couch,name,table,suburb_names):
    db = couch['bigtwitter']#bigtwitter
    view = db.view('_design/Tweet/_view/aurin_'+name,group=True)
    for row in view:
        area_name = str(row.key)
        # print(area_name)
        if( area_name in suburb_names):
            # print(area_name)
            table[area_name]['tweet_num'] = int(row.value)
    return table

# for education & language senario
def getAurinData(couch,name):
    db = couch['aurin_data_'+name+"_victoria"]
    suburb_names = ['','','','','','','','','','']
    count = 0
    table={}
    if(name=='education'):
        suburbs = [206041122,206041117,206071142,206051134,206061137,206061136,206051133,206051129,206031115,206051130]
        for id in db:
            doc = db[id]
            if(doc['properties']['area_code'] in suburbs):
                area_name=doc['properties']['area_name']
                suburb_names[count] = area_name
                count = count+1
                temp = {
                        'percentage':doc['properties']['16_years_old_education_percentage']
                    }
                table[str(area_name)]=temp
    else:
        suburbs = ['206041122','206041117','206071142','206051134','206061137','206061136','206051133','206051129','206031115','206051130']
        for id in db:
            doc = db[id]
            if(doc['properties']['SA2_MAIN11'] in suburbs):
                area_name = doc['properties']['SA2_NAME11']
                suburb_names[count] = area_name
                count = count+1
                english = int(doc['properties']['SEO_Persons'])
                other = int(doc['properties']['SOL_Tot_P'])
                total = english+other
                if(total!=0):
                    calculation = float(english)*100/float(total)
                else:
                    calculation = 0
                temp = {
                    'English':english,
                    'OtherSpeaker' :other,
                    'percentage': round(calculation,2)
                }
                table[str(area_name)]=temp
    return table,suburb_names

# calculating the percentage of enducation and language senario
def calulatePercentage(couch,name,table,suburb_names):
    db = couch['bigtwitter']#bigtwitter
    view = db.view('_design/Tweet/_view/aurin_'+name,group=True)
    if(name == 'education'):
        value_name = 'positive'
    else:
        value_name = 'en'
    for row in view:
        area_name = str(row.key[0])
        if(area_name in suburb_names):
            if(row.key[1]=='positive' or row.key[1]=='en'):
                table[area_name][value_name] = int(row.value)
            else:
                table[area_name]['other'] = int(row.value)
    #calculate percentage
    for item in table:
        if('other' in table[item]):
            if(value_name in table[item]):
                value = table[item][value_name]
                other = table[item]['other']
                calculation = float(value)*100/float(value+other)
                table[item]['calculation']=round(calculation,2)
        else:
            if(value_name in table[item]):
                value = table[item][value_name]
                table[item]['other'] = 0
                table[item]['calculation']=100
            else:
                table[item][value_name]=0
                table[item]['other']=0
                table[item]['calculation']=0
    return table

# add markers to aurin map
def aurinMapMarkers(markers):
    aurinmap = Map(
            identifier="aurinmap",
            varname="aurinmap",
            style=(
            "height:500px;"
            "width:700px;"
            "z-index:200;"
            ),
            lat=-37.8136,
            lng=144.9630,
            zoom=11,
            markers = markers,
            zoom_control=False,
            maptype_control=False,
            scale_control=False,
            streetview_control=False,
            rotate_control=False,
            fullscreen_control=False
        )
    return aurinmap

@app.route('/timemap')
def timemap():
    # connect to database
    db,couch = connect()
    # get senarios for searching in another page
    # senario = readSenario()
    # get top topics in different city
    melb,melbcount = getTimeTweets(db,'Melbourne')
    sydn,sydncount = getTimeTweets(db,'Sydney')
    pert,pertcount = getTimeTweets(db,'Perth')
    adel,adelcount = getTimeTweets(db,'Adelaide')

    melbcount = addWithBig(melbcount,couch,'Melbourne')
    sydncount = addWithBig(sydncount,couch,'Sydney')
    pertcount = addWithBig(pertcount,couch,'Perth')
    adelcount = addWithBig(adelcount,couch,'Adelaide')


    # set markers
    # markers = []
    # markers = addTimeMarker(markers,'Melbourne',melb,melbcount,[-37.8136,144.9630])
    # markers = addTimeMarker(markers,'Sydney',sydn,sydncount,[-33.8599,151.2111])
    # markers = addTimeMarker(markers,'Perth',pert,pertcount,[-31.9522,115.8589])
    # markers = addTimeMarker(markers,'Adelaide',adel,adelcount,[-34.9290,138.6010])
    # # draw a map
    # timemap = setTimeMap(markers)

    return render_template('time.html',
        melbchartvalue = melbcount, sydnchartvalue=sydncount,
        pertchartvalue = pertcount, adelchartvalue=adelcount)
 # 'https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=flag|bbT|Negative:'+str(value)+'|FFFFFF|FF0000'
# 'icon':'https://chart.googleapis.com/chart?chst=d_bubble_texts_big&chld=bbT|FFFFFF|000000|Melbourne+Topic|1.+A|2.+B|3.+C',
def getTimeTweets(db,city):
    # read part of the view from database, only focus on one city
    view = db.view('_design/Tweet/_view/timeslots',group=True,startkey=[city],endkey=[city,{}])
    # the results
    timeslopt=['','','','']
    num = [0,0,0,0]
    # find through the view
    for row in view:
        if(row.key[1]=='0-6'):
            timeslopt[0] = row.key[1]
            num[0] = row.value
        if(row.key[1]=='6-12'):
            timeslopt[1] = row.key[1]
            num[1] = row.value
        if(row.key[1]=='12-18'):
            timeslopt[2] = row.key[1]
            num[2] = row.value
        if(row.key[1]=='18-24'):
            timeslopt[3] = row.key[1]
            num[3] = row.value
    return timeslopt,num

def addWithBig(num,couch,city):
    db = couch['bigtwitter']
    view = db.view('_design/Tweet/_view/timeslots',group=True,startkey=[city],endkey=[city,{}])
    for row in view:
        if(row.key[1]=='0-6'):
            num[0] = num[0] + row.value
        if(row.key[1]=='6-12'):
            num[1] = num[1] + row.value
        if(row.key[1]=='12-18'):
            num[2] = num[2] + row.value
        if(row.key[1]=='18-24'):
            num[3] = num[3] + row.value
    return num

# set markers for the map
def addTimeMarker(markers,name,time,count,location):
    markers.extend([{
            'lat':location[0],
            'lng':location[1],
            'infobox':(
                "<b>"+name+":</b>"
                "<h4>"+time[0]+":"+str(count[0])+"</h4>"
                "<h4>"+time[1]+":"+str(count[1])+"</h4>"
                "<h4>"+time[2]+":"+str(count[2])+"</h4>"
                "<h4>"+time[3]+":"+str(count[3])+"</h4>"
            )
    }])
    return markers

# create topic map with markers
def setTimeMap(markers):
    timemap = Map(
            identifier="timemap",
            varname="timemap",
            style=(
            "height:500px;"
            "width:700px;"
            "z-index:200;"
            ),
            lat=-30.0000,
            lng=137.6010,
            zoom=4,
            markers = markers,
            zoom_control=False,
            maptype_control=False,
            scale_control=False,
            streetview_control=False,
            rotate_control=False,
            fullscreen_control=False
        )
    return timemap

if __name__ == "__main__":
    app.run(host='0.0.0.0')


	