#!/bin/sh

sudo killall -u couchdb

sudo rm -rf /mnt/myVolume/couchDBdata

sudo rm -rf /home/couchdb
sudo rm -rf /home/ubuntu/apache-couchdb-2.0.0
sudo rm -f /home/ubuntu/apache-couchdb-2.0.0.tar.gz

sudo rm -rf /var/log/couchdb
sudo rm -rf /etc/sv/couchdb
sudo rm -rf /etc/service/couchdb

sudo rm -rf /etc/systemd/system/couchdb.service

sudo rm -rf apache-couchdb-2.0.0
sudo rm -rf apache-couchdb-2.0.0.tar.gz

sudo userdel couchdb