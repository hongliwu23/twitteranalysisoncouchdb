"""
Define constant values to be used

@author: Jiawen

"""


class Params:

    """
        Threshold values used in sentiment analysis
    """
    POSITIVE_TWEET_THRESHOLD = 0.25
    NEGATIVE_TWEET_THRESHOLD = -0.25

    """
        Parameters for configuring CouchDB
    """
    COUCHDB_DBNAME_BIGTWITTER = 'bigtwitter'
    COUCHDB_DBNAME = 'twitter'
    COUCHDB_ACCOUNT = 'team22'
    COUCHDB_PASSWORD = 'group22'
    COUCHDB_ADDRESS = 'localhost'
    COUCHDB_PORT = '5984'

    """
        Geo info locations
    """
    SA2_GEO_DATA_FILE_PATH = './AURIN/SA2_Geo_Info.json'
    CITY_BOUNDED_BOX = './locations_info.txt'

    """
        Log file output path
    """
    SEARCH_LOG_FILE = '/mnt/myVolume/searchLog.txt'
    STREAMING_LOG_FILE = '/mnt/myVolume/streamingLog.txt'
