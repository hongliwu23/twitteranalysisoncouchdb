"""
Collecting twitter data by Twitter Search API

@author: Lining

"""

import tweepy
import json
from DataProcess.TweetProcessing import TweetProcessing
from Params import Params


class SearchCollection(object):
    """
        Constructor
    """

    def __init__(self, OAuth_array, dbHandler, SA2_Handler):
        self.dbHandler = dbHandler
        self.oauth_keys = OAuth_array
        self.auths = []
        self.apis = []
        self.SA2_Handler = SA2_Handler
        for consumer_key, consumer_secret, access_token, access_token_secret \
                in OAuth_array:
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
            auth = tweepy.AppAuthHandler(consumer_key, consumer_secret)
            # Creating a twitter API wrapper using tweepy
            # Details here http://docs.tweepy.org/en/v3.5.0/api.html
            api = tweepy.API(auth)
            if (not api):
                print("Problem connecting to API")
            self.auths.append(auth)
            self.apis.append(api)

    """
        Collecting twitter data
    """

    def collectingDataByPlaces(self, places, placeType):
        print("Start collecting data")

        # generate the search query by a list of place ids
        searchQuery = ''

        # get places id in twitter
        for placeId in places:
            searchQuery += 'place:%s OR ' % placeId

        # remove the last 'OR ' inserted
        searchQuery = searchQuery[:-3]
        print(searchQuery)

        # Switching to application authentication
        # for oauth_key, auth in zip(self.oauth_keys, self.auths):
        #     auth = tweepy.AppAuthHandler(oauth_key[0], oauth_key[1])
        #
        # # Setting up new api wrapper, using authentication only
        # for auth, api in zip(self.auths, self.apis):
        #     api = tweepy.API(auth)
        #     if (not api):
        #         print("Problem Connecting to API")

        # start searching
        api_num = 0
        api = self.apis[api_num]
        sinceid = 0
        count = 0
        with open(Params.SEARCH_LOG_FILE, 'a') as log:
            while True:
                try:
                    # use Cursor method to apply Search API for retrieving data
                    # also tell Cursor the query, and the maximum number of
                    # tweets to return
                    for tweet in tweepy.Cursor(api.search, q=searchQuery,
                                               since_id=sinceid,
                                               count=100).items():
                        sinceid = tweet._json['id']
                        if tweet._json['place']:
                            tweetPlaceId = tweet._json['place']['id']
                            if tweetPlaceId:
                                # processing the tweet
                                if tweetPlaceId in places:
                                    placeName = places[tweetPlaceId]
                                    processor = TweetProcessing(placeName,
                                                                self.SA2_Handler)
                                    jsonStr = json.dumps(tweet._json)
                                    document = processor.processingTweet(
                                        jsonStr)
                                    self.dbHandler.createDocument(
                                        Params.COUCHDB_DBNAME, document)
                                    count += 1
                                    log.write(str(count) + '\n')
                                    log.flush()
                except tweepy.TweepError:
                    # print e
                    api_num = (api_num + 1) % len(self.auths)
                    api = self.apis[api_num]
        log.close()
