"""
    Abstraction of the BoundedBox for representing the border of an area

    Each bounding box should be specified as a pair of longitude and latitude pairs, 
    with the southwest corner of the bounding box coming first.
    
    The scope of an area is then defined by the bounding box.

    @author: Jiawen
"""


class BoundedBox(object):
    """ 
        initialise a BoundedBox 
    """

    def __init__(self, name,
                 southwest_longitude, southwest_latitude,
                 northeast_longitude, northeast_latitude):
        self.name = name
        self.southwest_long = float(southwest_longitude)
        self.southwest_lat = float(southwest_latitude)
        self.northeast_long = float(northeast_longitude)
        self.northeast_lat = float(northeast_latitude)

    """
         Return an array of floats which contains a geo box info
        Return value format: 
        [southwest_longitude, southwest_latitude, northeast_longtitude, northeast_latitude]
    """

    def getGeoBoxArray(self):
        GeoBox = [self.southwest_long,
                  self.southwest_lat,
                  self.northeast_long,
                  self.northeast_lat]
        return GeoBox

    """
        Get city name of the give boxed area
    """
    def getCityName(self):
        return self.name

    """
        Check if a point is located in a given GeoBox
    """
    def isInBox(self, longtitude, latitude):
        # the longitude and latitude of the given point is in the area of this
        # GeoBox
        if longtitude <= self.northeast_long:
            if longtitude >= self.southwest_long:
                if latitude <= self.northeast_lat:
                    if latitude >= self.southwest_lat:
                        return True
        return False

