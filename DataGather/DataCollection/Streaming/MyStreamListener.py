"""
Implementing the StreamListener in the tweepy package for receiving data 
from streaming API

@author Jiawen

"""

from tweepy.streaming import StreamListener
from DataProcess.TweetProcessing import TweetProcessing
from Params import Params


class MyStreamListener(StreamListener):

    def __init__(self, cityName, dbHandler, SA2_handler):
        self.cityName = cityName
        self.dbHandler = dbHandler
        self.SA2_handler = SA2_handler
        # count the number of tweet objects retrieved
        self.tweetsCount = 0

    def on_data(self, data):
        try:
            # print("Get one tweet, start processing")
            # use the TwitterProcessing to handle the twitter data retrieved
            processor = TweetProcessing(self.cityName, self.SA2_handler)
            document = processor.processingTweet(data)
            # export the tweet json modified to CouchDB as a document
            result = self.dbHandler.createDocument(
                Params.COUCHDB_DBNAME, document)
            # add count for log
            self.tweetsCount += 1
            with open(Params.STREAMING_LOG_FILE, 'a') as log:
                log.write(str(self.tweetsCount) + '\n')
                log.flush()
            # print(result)
            return True
        except BaseException as e:
            log.close()
            print("on_data() Exception: %s" % str(e))
        return True

    # handling errors
    def on_error(self, status):
        print(status)
        # for more information on error codes from the twitter api
        # please refer to https://dev.twitter.com/overview/api/response-codes
        if status == 420:
            # clients exceed a limited number of attempts to
            # connect to the streaming API in a window of time
            print("Rate limit")
            # returning False disconnects the stream
            return False
        return True
