"""
    An implementation of collecting required data from Twitter by Streaming API
    
    @author: Jiawen
"""

from tweepy import OAuthHandler
from tweepy import Stream
from DataCollection.Streaming.MyStreamListener import MyStreamListener
from DataCollection.Streaming.BoundedBox import BoundedBox


class StreamingCollection(object):
    """
        Constructor
        Record access keys for accessing Twitter Streaming API
        as well as the database information for exporting data
    """
    def __init__(self, consumer_key, consumer_secret,
                 access_token, access_secret, dbHandler):
        self.auth = OAuthHandler(consumer_key, consumer_secret)
        self.auth.set_access_token(access_token, access_secret)
        self.dbHandler = dbHandler


    """
        Collecting tweets that are posted in the given area
        @param location: 
            an instance of GeoBox object which contains information of a city
    """
    def collectingByLocation(self, location, SA2_handler):

        # initialise the defined StreamListener
        myListener = MyStreamListener(location.getCityName(),
                                      self.dbHandler, SA2_handler)
        # create stream instance
        twitter_stream = Stream(self.auth, myListener)
        # filter tweets within the border of locations
        twitter_stream.filter(locations=location.getGeoBoxArray())
