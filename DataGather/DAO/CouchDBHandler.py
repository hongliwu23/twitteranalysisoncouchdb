"""
    Implementing basic CouchDB operations 
    
    @author Jiawen
"""
import couchdb


class CouchDBHandler(object):
    """
        Constructor
    """
    def __init__(self, account, password, address, port):
        serverAddress = "http://%s:%s@%s:%s/" % (account, password, address,
                                                 port)
        self.couchServer = couchdb.Server(serverAddress)
        # check the server availability
        print("Successfully connect to the CouchDB "
              "Server! Server version: % s" % self.couchServer.version())

    """
        Create a new database
        @:param dbName - the name of database to be created
    """
    def createDB(self, dbName):
        print("Request to create a database: %s" % dbName)
        try:
            db = self.couchServer.create(dbName)
            print("Successfully create a database: %s" % str(dbName))
        except Exception as e:
            print("createDB() Exception: %s" % str(e))

    """
        Delete a existing database
        @:param dbName - the name of database to be deleted
    """
    def deleteDB(self, dbName):
        print("Request to delete a database: %s" % dbName)
        try:
            del self.couchServer[dbName]
            print("Successfully delete a database: %s" % str(dbName))
        except Exception as e:
            print("deleteDB() Exception: %s" % str(e))

    """
        Retrieve database from the CouchDB server
        @:param dbName - the name of database to be retrieved
        @:return the corresponding couchdb.Database object
    """
    def getDB(self, dbName):
        try:
            db = self.couchServer[dbName]
            return db
        except Exception as e:
            print("getDB() exception raised: %s" % str(e))

    """
        Create a document and insert it into a database
        The ID of a document can be specified by including an _id item in the 
        document.
        
        @:param dbName - the name of database to add the document
        @:param document - the document data to add in Python dictionary type
        @:returns the ID and “rev” for the newly created document
    """
    def createDocument(self, dbName, document):
        try:
            # print("Database info: %s" % database.info)
            result = self.getDB(dbName).save(document)
            # print("Document created")
            return result
        except couchdb.ResourceConflict:
            # same document id has existed in database
            # update the document instead of create a new one
            docId = document['_id']
            self.updateDocument(dbName, docId, document, document.keys())
            # print("Existing Document found")
        except Exception as e:
            print("createDocument() exception: %s" % str(e))

    """
        Update a document
        
        @:param dbName - the database name where the document existed
        @:param documentId - the id of documents to be implemented
        @:param newDocument - the latest document data in dictionary type
        @:param updateKeys - a list of keys in the document which 
                            corresponding values need to be updated
    """
    def updateDocument(self, dbName, documentId, newDocument, updateKeys):
        # retrieve the database instance
        database = self.getDB(dbName)
        # retrieve the document with specified id from CouchDB
        doc = self.readDocument(dbName, documentId)
        try:
            # update the values for the document in database
            for key in list(updateKeys):
                doc[key] = newDocument[key]
            # print("Document updated")
            database[documentId] = doc
        except Exception as e:
            print("updateDocument() exception: %s" % str(e))


    """
        Delete a document
        @:param dbName - the name of database to delete the document
        @:param documentId - the id of document to be deleted
    """
    def deleteDocument(self, dbName, documentId):
        try:
            database = self.getDB(dbName)
            del database[documentId]
        except Exception as e:
            print("deleteDocument() Exception: %s" % str(e))

    """
        Retrieve information of a document from CouchDB
        @:param dbName - the name of database where the document is in
        @:param documentId - the id of document to be retrieved
        @:return document information get from CouchDB
    """
    def readDocument(self, dbName, documentId):
        try:
            database = self.getDB(dbName)
            doc = database[documentId]
            return doc
        except Exception as e:
            print("readDocument() Exception:%s" % str(e))

    """
        Creating a view in CouchDB
    """

    def createView(self):
        pass

    """
        Retrieving a view from CouchDB
    """

    def retrieveView(self):
        pass
