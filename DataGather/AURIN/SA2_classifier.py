"""
 Module for classifying area in terms of SA2
 
 @author: Hao Yu
"""
from shapely.geometry import Point
from shapely.geometry import Polygon
import json


class SA2_ClassifierHandler(object):
	"""
		Constructor
		
	"""
	def __init__(self, file_name):
		#read info
		data = open(file_name, encoding='utf-8').read()
		json_data = json.loads(data)
		self.locs = []
		
		#extract location info
		features = json_data["features"]
		
		#only consider top 10 population density SA2 area
		total = len(features)
		top10 = list(range(total, total-10, -1))
		
		#store top10 location information
		for feature in features:
			property = feature["properties"]
			if property["rank"] in top10:
				SA2_code = property["sa2_main11"]
				SA2_name = property["sa2_name11"]
				geo = feature["geometry"]
				coordinates = geo["coordinates"][0][0]
				poly = Polygon(coordinates)
				self.locs.append((SA2_code, SA2_name, poly))
				top10.remove(property["rank"])
		print(len(self.locs), len(top10))

	def findLocation(self, coordinate):
		for loc in self.locs:
			if loc[2].contains(Point(coordinate)):
				#return (area_code, area_name) as tuple
				return((loc[0], loc[1]))
		return(None)


"""
Test the SA2_ClassifierHandler module
"""
if __name__ == '__main__':
	handler = SA2_ClassifierHandler("SA2_Geo_Info.json")
	loc = handler.findLocation((145, -36))
	print(loc)
