"""
Main script for gathering the twitter historical data by Search API

@author Lining
"""
import os
import sys
from DataCollection.Search.SearchCollection import SearchCollection
from DAO.CouchDBHandler import CouchDBHandler
from Params import Params
from AURIN.SA2_classifier import SA2_ClassifierHandler

if __name__ == '__main__':
    # get process id of this program
    pid = str(os.getpid())
    print(pid)

    # store the path of file which contains OAuth access keys
    accountsFile = "OAuth_info.txt"
    # store the path of file which contains locations information
    locationsFile = "search_locations.txt"

    # handling command-line arguments
    if len(sys.argv) > 1:
        accountsFile = sys.argv[1]
    if len(sys.argv) > 1:
        locationsFile = sys.argv[1]

    # store OAuth access keys information
    oauth_keys = []
    # retrieve OAuth keys information
    with open(accountsFile, 'r') as accounts:
        for line, info in enumerate(accounts):
            if line > 0:
                # start from the second line
                values = info.rstrip("\n").split("\t")
                if 'search' in values[0]:
                    # print(values[0])
                    # OAuth keys used for Search API
                    oauth_keys.append([values[1], values[2], values[3],
                                       values[4]])

    # store name and if of locations to be harvested
    places = {}
    # retrieve locations information
    with open(locationsFile, 'r') as locations:
        for line, info in enumerate(locations):
            if line > 0:
                values = info.rstrip("\n").split("\t")
                # store the location name and its id
                #print(values)
                places[values[1]] = values[0]
    #print(places)

    couchDBHandler = CouchDBHandler(Params.COUCHDB_ACCOUNT,
                                    Params.COUCHDB_PASSWORD,
                                    Params.COUCHDB_ADDRESS,
                                    Params.COUCHDB_PORT)

    SA2_handler = SA2_ClassifierHandler(Params.SA2_GEO_DATA_FILE_PATH)
    search = SearchCollection(oauth_keys, couchDBHandler, SA2_handler)
    search.collectingDataByPlaces(places, 'city')
