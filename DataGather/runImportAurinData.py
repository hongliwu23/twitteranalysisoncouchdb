"""
Script used for importing all aurin data in the AURIN_DATA_ROOT to 
the CouchDB

@author: Hao Yu
"""
import os
import json
from DAO.CouchDBHandler import CouchDBHandler
from Params import Params
	
def importData(couchDBHandler, DBname, json_data):
	#create new DB
	couchDBHandler.createDB(DBname)
	
	#import data
	features = json_data["features"]
	for feature in features:
		couchDBHandler.createDocument(DBname, feature)
	
if __name__ == '__main__':
	root = Params.AURIN_DATA_ROOT
	
	#connect to DB
	couchDBHandler = CouchDBHandler(Params.COUCHDB_ACCOUNT,
                                    Params.COUCHDB_PASSWORD,
                                    Params.COUCHDB_ADDRESS,
                                    Params.COUCHDB_PORT)
									
	for file in os.listdir(root):
		DBname = "aurin_"+os.path.splitext(file)[0]+"_victoria"
		DBname = DBname.lower()
	
		#read info
		data = open(os.path.join(root, file), encoding='utf-8').read()
		json_data = json.loads(data)
		
		importData(couchDBHandler, DBname, json_data)
		
