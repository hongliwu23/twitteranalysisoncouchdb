"""
Process the tweet json data by applying analysis to the data then export it 
to the database

@author: Jiawen

"""
import json
from DataProcess.TweetAnalysis import TweetAnalysis
from DataProcess.TweetDataHandler import TwitterDataHandling

class TweetProcessing(object):
    """
        Constructor of the TwitterProcessing class
    """
    def __init__(self, cityname, SA2_handler):
        self.cityName = cityname
        self.SA2_handler = SA2_handler

    """
        Process a tweet json string by analysing the tweet data
        and output the result
    """
    def processingTweet(self, tweetJson):
        # retrieve the tweet text
        tweetData = json.loads(tweetJson)
        # performing analysis to the given tweet text
        analyser = TweetAnalysis(tweetData)
        # performing sentiment analysis
        sentimentResult = analyser.sentimentAnalysis()
        # retrieve the topics in the tweet
        topics = analyser.retrieveTopics()
        # print(topics)
        # retrieve the corresponding SA2 area information from AURIN
        SA2_location = analyser.in_SA2_Area(self.cityName, self.SA2_handler)

        # get the time slot of the tweet created
        timeSlot = analyser.getCreatePeriod()

        # append analysis result to the original tweet json object
        dataHandler = TwitterDataHandling(tweetJson)
        dataHandler.appendItemToJson("sentiment", sentimentResult)
        dataHandler.appendItemToJson("topics", topics)
        dataHandler.appendItemToJson("city", self.cityName)
        dataHandler.appendItemToJson("SA2_Location", SA2_location)
        dataHandler.appendItemToJson("created_time_slot", timeSlot)

        document = dataHandler.getTweetDocument()
        # print(document)
        return document
