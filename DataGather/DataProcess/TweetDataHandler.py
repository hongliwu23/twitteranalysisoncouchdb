"""
    Implement operations needed for modifying a tweet JSON object, including 
    appending new key-value pair into the JSON object and adjust the JSON 
    object to make it ready to save as a document in CouchDB
    
    @author: Jiawen
"""
import json

class TwitterDataHandling(object):

    """
        Constructor
    """
    def __init__(self, tweetJson):
        # Store the tweet JSON to be modified
        self.tweet = tweetJson

    """
        append extra information to the given tweet json
    """
    def appendItemToJson(self, key, value):
        # generate the new json with the key-value pair
        data = json.loads(self.tweet)
        data[key] = value
        self.tweet = json.dumps(data)

    """
        Return a CouchDB Document with the tweet json data inside
        Use the tweet id as the document id for filtering duplicate data
    """
    def getTweetDocument(self):
        # convert the json object to Python dict
        data = json.loads(self.tweet)
        # remove the tweet id in integer format
        data.pop("id")
        # replace the "id_str" key with "_id"
        # to make the tweet id can be set as the document id
        # use the tweet id as document id for filtering duplicate tweets
        data['_id'] = data.pop('id_str')
        #print(data['_id'])
        # There is no need to convert the dictionary to json object
        # by using json.dumps(data), exception would occur: "String indices
        # must be integers". Therefore, directly return the dictionary type
        return data
