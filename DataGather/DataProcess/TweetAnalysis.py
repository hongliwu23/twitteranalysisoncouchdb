"""
Perform related analysis on tweets
Only implement sentiment analysis in current implementation

@author: Jiawen

"""
import time
# Use textblob to analysing the tweet content
from textblob import TextBlob
from Params import Params
from AURIN.SA2_classifier import SA2_ClassifierHandler


class TweetAnalysis(object):

    """
        Constructor
        @:param tweetData - the tweet data to be analysed
    """
    def __init__(self, tweetData):
        self.tweetData = tweetData

    """
        Perform sentiment analysis to the tweet. The sentiment analysis 
        first evaluates the polarity of a tweet by making use of the 
        textblob package, then determine whether the tweet present a 
        positive attitude or not by comparing the polarity to pre-defined 
        thresholds.
    """
    def sentimentAnalysis(self):
        tweetText = self.tweetData['text']
        content = TextBlob(tweetText)
        # print (content.sentiment)
        polarity = content.sentiment.polarity
        # print (polarity)
        # check if the attitude of a content by polarity
        if polarity >= Params.POSITIVE_TWEET_THRESHOLD:
            return "positive"
        elif polarity <= Params.NEGATIVE_TWEET_THRESHOLD:
            return "negative"
        else:
            return "neutral"

    """
        Find out topics contained in the tweet by retrieving hashtags content
        Note that if the tweet is a re-tweet, hashtags in its retweeted status 
        would also be included.
    """
    def retrieveTopics(self):
        # store the topics of a tweet
        topics = []
        # retrieve the entities data field from the tweet object
        tweetEntities = self.tweetData['entities']
        hashtags = tweetEntities['hashtags']
        # add the content of each hashtag
        for record in hashtags:
            topics.append(record['text'])

        # check if this tweet is a re-tweet
        isRetweeted = self.tweetData['retweeted']
        if isRetweeted:
            print("Found one retweeted record.")
            # retrieve the topics of original tweet
            retweetedStatus = self.tweetData['retweeted_status']
            originalHashtags = retweetedStatus['entities']['hashtags']
            print("Original Hashtags: %" % originalHashtags)
            # add the content to topic records
            for record in originalHashtags:
                topics.append(record['text'])

        return topics

    """
        Get the SA2 level of areas information from AURIN data and 
        check if a tweet is sent from one of the suburbs provided by 
        checking its coordinate.
         
        @:return If a tweet has coordinates information and its location is 
        located inside a given area, return (area_id, area_name); 
        Otherwise, if a tweet does not contain coordinate or its coordinate 
        does not locate in the given areas, return None.
        areas.
    """
    def in_SA2_Area(self, cityName, SA2_handler):
        if self.tweetData['coordinates']:
            # if the tweet has coordinate information
            if cityName == 'Melbourne':
                coordinate = self.tweetData['coordinates']['coordinates']
                loc = SA2_handler.findLocation((coordinate[0], coordinate[1]))
                return loc
            else:
                # no filter provided for other cities in current AURIN module
                return None
        else:
            return None

    """
        Retrieve the time when the tweet was created and return the 
        corresponding time slots.
        
        For tweet created from 0:00 to 5:59, return 0-6;
        For tweet created from 6:00 to 11:59, return 6-12;
        For tweet created from 12:00 to 17:59, return 12-18;
        For tweet created from 18:00 to 23:59, return 18-24
    """
    def getCreatePeriod(self):
        # timestamp example: "Wed Aug 27 13:08:45 +0000 2008"
        timeStr = self.tweetData['created_at']
        timePattern = "%a %b %d %H:%M:%S +0000 %Y"
        timeRecords = time.strptime(timeStr, timePattern)
        hour = timeRecords.tm_hour
        if 0 <= hour < 6:
            return '0-6'
        elif 6 <= hour < 12:
            return '6-12'
        elif 12 <= hour < 18:
            return '12-18'
        elif 18 <= hour < 24:
            return '18-24'
