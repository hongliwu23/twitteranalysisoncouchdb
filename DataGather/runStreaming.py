"""
Main script for gathering the twitter real time data

Run this script by using following command:
    python3 runStreaming.py 
    with the following 4 arguments:
    [OAuth_Account_Name] [city] [OAuth_Info_FilePath] [CityInfo_FilePath]
e.g. 
    python3 runStreaming.py jiawen Melbourne OAuth_info.txt locations_info.txt
    
@author: Jiawen
"""
import os
import sys

from DataCollection.Streaming.StreamingCollection import StreamingCollection
from DataCollection.Streaming.BoundedBox import BoundedBox
from DAO.CouchDBHandler import CouchDBHandler
from Params import Params
from AURIN.SA2_classifier import SA2_ClassifierHandler

if __name__ == '__main__':
    # get process id of this program
    pid = str(os.getpid())
    print(pid)
    # initialise arguments
    accountName = ""
    cityToHarvest = ""
    accountsFile = "OAuth_info.txt"
    locationsFile = "locations_info.txt"

    # handling command-line arguments
    if len(sys.argv) > 1:
        accountName = sys.argv[1]
    if len(sys.argv) > 2:
        # the name of city to be harvested
        cityToHarvest = sys.argv[2]
        print(cityToHarvest)
    if len(sys.argv) > 3:
        accountsFile = sys.argv[3]
    if len(sys.argv) > 4:
        locationsFile = sys.argv[4]

    # initialise the object for handling CouchDB
    couchDBHandler = CouchDBHandler(Params.COUCHDB_ACCOUNT,
                                    Params.COUCHDB_PASSWORD,
                                    Params.COUCHDB_ADDRESS,
                                    Params.COUCHDB_PORT)

    # an instance of StreamingCollection object to harvest streaming data
    # initialise with empty values for access keys
    streamCollector = None

    # read the account file and retrieve the access keys
    with open(accountsFile, 'r') as accountRecords:
        for line, info in enumerate(accountRecords):
            # read from second line as the first line only contains titles
            if line > 0:
                values = info.rstrip("\n").split("\t")
                # print(values)
                # if accountName has been specified
                if accountName:
                    # find the target account information
                    if values[0] == accountName:
                        streamCollector = StreamingCollection(values[1],
                                                              values[2],
                                                              values[3],
                                                              values[4],
                                                              couchDBHandler)

    if streamCollector == None:
        print("Cannot retrieve access keys. Please check account info...")
        sys.exit(0)
    else:
        SA2_handler = SA2_ClassifierHandler(Params.SA2_GEO_DATA_FILE_PATH)
        # retrieve the bounding boxes for geolocations
        with open(locationsFile, 'r') as geoRecords:
            for line, info in enumerate(geoRecords):
                if line > 0:
                    # retrive city informantion in current line
                    values = info.rstrip("\n").split("\t")
                    # the first item is the city name
                    if values[0] == cityToHarvest:
                        # this city is one of the target cities
                        print("Target City found:%s" % values)
                        geoBox = BoundedBox(values[0], values[1],
                                            values[2], values[3], values[4])
                        # start collecting streaming data
                        streamCollector.collectingByLocation(geoBox,
                                                             SA2_handler)
