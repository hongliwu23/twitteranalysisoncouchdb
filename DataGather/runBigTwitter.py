"""
Script used for importing existing twitter data in the bigTwitter.json file to 
the CouchDB

@author: Jiawen
"""
import os
import sys
import json
from DAO.CouchDBHandler import CouchDBHandler
from Params import Params
from DataProcess.TweetProcessing import TweetProcessing
from DataCollection.Streaming.BoundedBox import BoundedBox
from AURIN.SA2_classifier import SA2_ClassifierHandler

if __name__ == '__main__':
    # get process id of this program
    pid = str(os.getpid())
    print("Process id: %s" % pid)

    dataPath = None
    # handling command-line arguments
    if len(sys.argv) > 1:
        dataPath = sys.argv[1]

    if dataPath is None:
        print('No data path provided')
        sys.exit(0)

    # initialise the object for handling CouchDB
    couchDBHandler = CouchDBHandler(Params.COUCHDB_ACCOUNT,
                                    Params.COUCHDB_PASSWORD,
                                    Params.COUCHDB_ADDRESS,
                                    Params.COUCHDB_PORT)

    # retrieve the bounding boxes for geolocations
    cityBoxes = []
    with open(Params.CITY_BOUNDED_BOX, 'r') as geoRecords:
        for line, info in enumerate(geoRecords):
            if line > 0:
                # retrive city informantion in current line
                values = info.rstrip("\n").split("\t")
                geoBox = BoundedBox(values[0], values[1],
                                    values[2], values[3], values[4])
                cityBoxes.append(geoBox)

    SA2_handler = SA2_ClassifierHandler(Params.SA2_GEO_DATA_FILE_PATH)

    with open('log.txt', 'a') as log:
        with open(dataPath, 'r') as file:
            # read each line of the given file
            for line, content in enumerate(file):
                log.write(str(line) + "\n")
                log.flush()
                # json object start with "{"
                if content[0] == "{":
                    tweetJson = None
                    if content[-2] != "}":
                        # the last two characters in this line is not "} "
                        # which means this line is end with separator string ", "
                        # so this is not the last json object in the json array
                        tweetJson = json.loads(content[:-2])['json']
                    else:
                        # this is the last json object in the json array
                        tweetJson = json.loads(content)['json']
                    coordinates = tweetJson['coordinates']['coordinates']

                    for cityBox in cityBoxes:
                        if cityBox.isInBox(float(coordinates[0]),
                                           float(coordinates[1])):
                            # the coordinates locate inside the city box area
                            # retrieve the city name
                            city = cityBox.getCityName()
                            # print(city)
                            # initialise TweetProcessing for processing data
                            dataProcessor = TweetProcessing(city, SA2_handler)
                            document = dataProcessor.processingTweet(
                                                        json.dumps(tweetJson))
                            # print("Import one record.")
                            couchDBHandler.createDocument(
                                Params.COUCHDB_DBNAME_BIGTWITTER, document)
        file.close()
    log.close()

    print("Congratulation! Successfully export the data to CouchDB!")
