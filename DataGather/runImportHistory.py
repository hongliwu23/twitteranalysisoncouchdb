"""
Script used for importing existing twitter data in exported files to 
the CouchDB. The data was also retrieved from Twitter API, however, the data 
was temporarily stored in .json files before CouchDB is ready. Therefore, 
use this script to import these kinds of data into the CouchDB

Run this script by using following command:
    python3 runHistoryJson.py 
    with the following 2 arguments:
    [city] [exportFilePath] 
e.g. 
    python3 runStreaming.py Melbourne export_data.json
    
@author: Jiawen

"""
import os
import sys
from DAO.CouchDBHandler import CouchDBHandler
from Params import Params
from DataProcess.TweetProcessing import TweetProcessing
from AURIN.SA2_classifier import SA2_ClassifierHandler

if __name__ == '__main__':
    # get process id of this program
    pid = str(os.getpid())
    print("Process id: %s" % pid)

    city = None
    dataPath = None
    # handling command-line arguments
    if len(sys.argv) > 1:
        city = sys.argv[1]
    if len(sys.argv) > 2:
        dataPath = sys.argv[2]

    if city == None:
        print('No city name provided!')
        sys.exit(0)

    if dataPath == None:
        print('No data path provided')
        sys.exit(0)

    # initialise the object for handling CouchDB
    couchDBHandler = CouchDBHandler(Params.COUCHDB_ACCOUNT,
                                    Params.COUCHDB_PASSWORD,
                                    Params.COUCHDB_ADDRESS,
                                    Params.COUCHDB_PORT)

    SA2_handler = SA2_ClassifierHandler(Params.SA2_GEO_DATA_FILE_PATH)
    # initialise an instance of TweetProcessing for processing each tweet data
    dataProcessor = TweetProcessing(city, SA2_handler)

    with open(dataPath, 'r') as data:
        print("Successfully open data file! Please wait while importing data.")
        for line, tweetJson in enumerate(data):
            document = dataProcessor.processingTweet(tweetJson)
            # print("Import one record.")
            couchDBHandler.createDocument(Params.COUCHDB_DBNAME, document)
    print("Congratulation! Successfully export the data to CouchDB!")
